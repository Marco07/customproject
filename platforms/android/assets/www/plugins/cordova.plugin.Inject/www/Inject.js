
cordova.define("cordova.plugin.Inject.Inject", function(require, exports, module) {
var exec = require('cordova/exec');

// exports.coolMethod = function(arg0, success, error) {
//     exec(success, error, "Inject", "coolMethod", [arg0]);
// };

function Inject(){
}
Inject.prototype.optionsBuilder = function () {

  // defaults
  var message = null;
  var duration = "short";
  var position = "center";
  var addPixelsY = 0;

  return {
    withMessage: function(m) {
      message = m.toString();
      return this;
    },

    withDuration: function(d) {
      duration = d.toString();
      return this;
    },

    withPosition: function(p) {
      position = p;
      return this;
    },

    withAddPixelsY: function(y) {
      addPixelsY = y;
      return this;
    },

    build: function() {
      return {
        message: message,
        duration: duration,
        position: position,
        addPixelsY: addPixelsY
      };
    }
  };
};

Inject.prototype.coolMethod = function (options, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "Inject", "coolMethod", []);
};

Inject.prototype.showWithOptions = function (options, successCallback, errorCallback) {
  options.duration = (options.duration === undefined ? 'long' : options.duration.toString());
  options.message = options.message.toString();
  cordova.exec(successCallback, errorCallback, "Toast", "show", [options]);
};
Inject.prototype.show = function (message, duration, position, successCallback, errorCallback) {
  this.showWithOptions(
      this.optionsBuilder()
          .withMessage(message)
          .withDuration(duration)
          .withPosition(position)
          .build(),
      successCallback,
      errorCallback);
};

Inject.prototype.showShortTop = function (message, successCallback, errorCallback) {
  this.show(message, "short", "top", successCallback, errorCallback);
 console.log(message);
 // cordova.exec(successCallback, errorCallback, "Inject", "coolMethod", []);

};

Inject.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.Inject = new Inject();
  return window.plugins.Inject;
};

cordova.addConstructor(Inject.install);

});

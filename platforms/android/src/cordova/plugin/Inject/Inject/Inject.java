package cordova.plugin.Inject;

import android.util.Log;
import android.view.Gravity;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class Inject extends CordovaPlugin {
  private static final String ACTION_SHOW_EVENT = "show";
  private boolean isPaused;
  private static final int GRAVITY_CENTER = Gravity.CENTER_VERTICAL;
  private static final String TAG = "InjectCordovaPlugin";
  String messageReceived;
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        Log.v(TAG, "execute , action =" + action);
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }else if (ACTION_SHOW_EVENT.equals(action)) {
          if (this.isPaused) {
            return true;
          }
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
      messageReceived = message;
        if (message != null && message.length() > 0) {
          cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {

            final android.widget.Toast toast = android.widget.Toast.makeText(
              cordova.getActivity().getWindow().getContext(),
              messageReceived,
              android.widget.Toast.LENGTH_LONG
                );
                toast.setGravity(GRAVITY_CENTER, 0, 0);
                toast.show();
            }
            });
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}

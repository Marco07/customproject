import { Component } from '@angular/core';

import { NavController, Platform } from 'ionic-angular';

declare var window: any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  x: any;
  prueba: string;
  constructor(public navCtrl: NavController, private platform: Platform) {

  }

  methodTest(message){
    this.x = message;
    // console.log(this.x);
  }

  showToast(message, position) {
    // console.log(window.plugins.device.getInfo);
    // var success = function(message){
    //   console.log("mensaje en success")
    //   console.log(message);
    //   // if(message.message) this.methodTest(message);
    // }

    // var failure = function() {
    //     alert("Error calling Hello Plugin");
    // }

        // hello.greet("World", success, failure);
    // window.plugins.toast.showShortTop('Hello there!', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})
    window.plugins.Inject.showShortTop('Yo Man! Plugin Its working!', function(a){
      console.log(a)
      let myContainer = <HTMLElement> document.querySelector("#myDiv");
      if(a.message) myContainer.innerHTML = '<h1>'+ a.message +  '</h1>';
    },
      function(b){alert('toast error: ' + b)}
    );

    // var store = [];
    // var oldf = console.log;
    // console.log = function(){
    //    store.push(arguments);
    //    oldf.apply(console, arguments);
    // }
    //
    // console.log(store)
    // for(var val in store){
    //   console.log("esto es del for")
    //   console.log(val)
    //   if(val.hasOwnProperty(message)){
    //     this.x.push(val);
    //     this.prueba = "mensaje de prueba";
    //     console.log("esto es del if")
    //     console.log(this.x)
    //
    //   }
    // }
    // // console.log(store)
    // console.log("x antes del metodo")
    // console.log(this.x)
    //
    // this.otroMetodo(this.x);
        //   this.platform.ready().then(() => {
        //   });
        // window.plugins.toast.showShortTop('Hello there!', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})
  }

  // otroMetodo(valor) {
  //   console.log("Este es el del valor");
  //   console.log(valor);
  //   this.x = valor;
  //   this.prueba = "valor";
  // }


}
